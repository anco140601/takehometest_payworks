/*
 * mpos-ui : http://www.payworksmobile.com
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 payworks GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

package io.mpos.ui.tester;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.math.BigDecimal;
import java.util.EnumSet;

import io.mpos.Mpos;
import io.mpos.accessories.AccessoryFamily;
import io.mpos.accessories.parameters.AccessoryParameters;
import io.mpos.errors.MposError;
import io.mpos.provider.ProviderMode;
import io.mpos.transactions.Currency;
import io.mpos.transactions.Transaction;
import io.mpos.transactions.TransactionType;
import io.mpos.transactions.parameters.TransactionParameters;
import io.mpos.ui.paybutton.view.TransactionActivity;
import io.mpos.ui.shared.MposUi;
import io.mpos.ui.shared.model.MposUiConfiguration;
import io.mpos.ui.shared.util.UiHelper;

import static android.view.View.OnClickListener;

public class CheckoutActivity extends AppCompatActivity {
    private final static String MERCHANT_ID = "<create a test merchant in the gateway manager>";
    private final static String MERCHANT_SECRET = "<create a test merchant in the gateway manager>";

    private String mLastTransactionIdentifier = null;
    private TransactionParameters mLastTransactionParameters = null;
    private BigDecimal mLastTransactionAmount = BigDecimal.ZERO;


    private String txtPaymentAmount;
    private Double doublePaymentAmount;

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_checkout);

        MposUi.initialize(CheckoutActivity.this, ProviderMode.TEST, MERCHANT_ID, MERCHANT_SECRET);


        findViewById(R.id.charge_transaction).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                initMockPaymentController();

                txtPaymentAmount = ((EditText) findViewById(R.id.payment_amount)).getText().toString();
                doublePaymentAmount = Double.valueOf(txtPaymentAmount).doubleValue();

                //Styling the Payment Controller.
                MposUi.getInitializedInstance()
                        .getConfiguration().getAppearance()
                        .setColorPrimary(Color.parseColor("#ff9800"))
                        .setColorPrimaryDark(Color.parseColor("#f57c00"))
                        .setBackgroundColor(Color.parseColor("#FFF3E0"))
                        .setTextColorPrimary(Color.BLACK);
                startPayment(doublePaymentAmount);
            }
        });

        findViewById(R.id.refund_last_transaction).setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                TransactionParameters transactionParameters = new TransactionParameters.Builder().refund(mLastTransactionIdentifier).
                        subject("subject-refund").
                        build();
                Intent refundIntent = MposUi.getInitializedInstance().createTransactionIntent(transactionParameters);
                startActivity(refundIntent);
            }
        });

        updateViews();
    }

    void updateViews() {
        findViewById(R.id.refund_last_transaction).setVisibility(mLastTransactionIdentifier != null ? View.VISIBLE : View.GONE);
    }

    void initMockPaymentController() {
        MposUi mposUi = MposUi.initialize(this, ProviderMode.MOCK, "mock", "mock");
        AccessoryParameters mockAccessoryParameters = new AccessoryParameters.Builder(AccessoryFamily.MOCK).mocked().build();
        mposUi.getConfiguration().setTerminalParameters(mockAccessoryParameters);
        mposUi.getConfiguration().setSummaryFeatures(EnumSet.allOf(MposUiConfiguration.SummaryFeature.class));
    }

    void startPayment(double amount) {
        TransactionParameters params = new TransactionParameters.Builder().
                charge(BigDecimal.valueOf(amount), Currency.EUR).
                subject("subject").
                customIdentifier("customId").
                build();
        Intent intent = MposUi.getInitializedInstance().createTransactionIntent(params);
        startActivityForResult(intent, MposUi.REQUEST_CODE_PAYMENT);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == MposUi.REQUEST_CODE_PAYMENT) {
            if (resultCode == MposUi.RESULT_CODE_APPROVED) {
                Toast.makeText(this, "Transaction Approved", Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this, "Transaction Failed", Toast.LENGTH_SHORT).show();
            }

            mLastTransactionIdentifier = data.getStringExtra(MposUi.RESULT_EXTRA_TRANSACTION_IDENTIFIER);
            updateViews();
        }

        // an error could have occurred regardless of the main outcome
        // e.g. a transaction was successful but there was an error when sending the receipt
        MposError error = MposUi.getInitializedInstance().getError();
        if (error != null) {
            Toast.makeText(this, "An error occurred: " + error.getInfo(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_info, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_info) {
            new InfoDialog().show(getSupportFragmentManager(), "INFO");
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public static class InfoDialog extends DialogFragment {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {
            // Get app version
            LayoutInflater layoutInflater = getActivity().getLayoutInflater();
            View rootView = layoutInflater.inflate(R.layout.dialog_info, null);
            ((TextView) rootView.findViewById(R.id.info_sdk_version)).setText("SDK Version: " + Mpos.getVersion());
            ((TextView) rootView.findViewById(R.id.info_body)).setText(Html.fromHtml(getString(R.string.info_body)));
            ((TextView) rootView.findViewById(R.id.info_body)).setMovementMethod(LinkMovementMethod.getInstance());

            return new AlertDialog.Builder(getActivity(), getTheme())
                    .setView(rootView)
                    .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int i) {
                            dialog.dismiss();
                        }
                    }).create();
        }
    }

}
